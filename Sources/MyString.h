#pragma once
#include <string>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <istream>
#include <iostream>
#include <ostream>
#include <cassert>
#include <stdexcept>
#include <initializer_list>


namespace MyString
{

using std::ostream;
using std::exception;

class String {
public:
	class iterator;
	class reverse_iterator;
	// iterators
	iterator begin();
	iterator end();
	iterator cbegin() const;
	iterator cend() const;
	reverse_iterator rbegin();
	reverse_iterator rend();
	const reverse_iterator crbegin() const;
	const reverse_iterator crend() const;
	//constructors
	String();
	String(const std::initializer_list<char>& input_data);
	String(const char* input_data);
	String(const char* input_data, const int count);
	String(const String& input_data);
	String(const int, const char);

	//destructor
	~String();

	// operators
	String& operator+=(const char* string_);
	String& operator+=(std::string string_);
	String& operator+=(const String& other);

	String& operator=(const String& other);
	String& operator=(const char* string_);
	String& operator=(std::string string_);
	String& operator=(char ch);

	const char operator[](const int& index) const;
	char& operator[](const int& index);
	bool operator== (const char* lhs) const;

	// methods
	void push_back(const char& value);
	iterator insert(iterator position, const char& value);
	iterator erase(iterator it);
	iterator erase(iterator lhs, iterator rhs);
	void insert(iterator position, int count, const char& value);
	void pop_back();
	char& at(const int& index);
	char* data() const;
	void shrink_to_fit();
	void clear();
	void resize(const int& size_);
	void reserve(const int& capacity_);
	char& back();
	char& front();
	const char at(const int& index) const;
	const char back() const;
	const char front() const;
	char* c_str() const;
	int size() const;
	int capacity() const;
	int length() const;
	bool empty() const;


	void insert(int index, int count, char ch);
	void insert(int index, const char* string_);
	void insert(int index, const char* string_, int count);
	void insert(int index, std::string string_);
	void insert(int index, std::string string_, int count);

	void (*ins1)(int, int, char);
	void (*ins2)(int, const char*);
	void (*ins3)(int, const char*, int);
	void (*ins4)(int, std::string);
	void (*ins5)(int, std::string, int);

	void erase(int index, int count);
	void (*erase1) (int, int);

	void append(int count, char ch);
	void append(const char* string_);
	void append(const char* string_, int index, int count);
	void append(std::string string_);
	void append(std::string string_, int index, int count);

	void (*append1)(int, char);
	void (*append2)(const char*);
	void (*append3)(const char*, int, int);
	void (*append4)(std::string);
	void (*append5)(std::string, int, int);

	void replace(int index, int count, const char* string_);
	void replace(int index, int count, std::string string_);

	void (*replace1)(int, int, const char*);
	void (*replace2)(int, int, std::string);

	int getSize(const char* string_);

	int find(const char* substring, const int index) const;
	int find(const std::string substring, const int index) const;
	int find(const char* substring) const;

	void copy_string(const char* input_data);
	int value_capacity(int value);

	String substr(const int index) const;
	String substr(const int index, const int count) const;

private:
	char* _array;
	int _size;
	int _capacity;
};

std::ostream& operator<<(std::ostream& os, const String& string_);
//std::istream& operator>>(std::istream& is, String& string_);
bool operator<(const String& lhs, const String& rhs);
bool operator>(const String& lhs, const String& rhs);
bool operator!=(const String& lhs, const String& rhs);
bool operator<=(const String& lhs, const String& rhs);
bool operator>=(const String& lhs, const String& rhs);
String operator+(String lhs, String rhs);
String operator+(String lhs, const char* rhs);
String operator+(String lhs, std::string rhs);

class String::iterator {
public:
	iterator(char* value) : element(value) {}
	iterator& operator++() { element++; return *this; }
	iterator& operator--() { element--; return *this; }
	char& operator*() { return *element; }
	bool operator== (const iterator& other) const { return element == other.element; }
	bool operator!= (const iterator& other) { return element != other.element; }
	friend int operator-(iterator lhs, iterator rhs) { return lhs.element - rhs.element; }
	iterator operator+(int pos) { return iterator(element + pos); }
	iterator& operator=(iterator& other) { element = other.element; return *this; }
	char* element;
};

class String::reverse_iterator {
public:
	reverse_iterator(char* value) : element(value) {}
	reverse_iterator& operator++() { element--; return *this; }
	reverse_iterator& operator--() { element++; return *this; }
	char& operator*() { return *element; }
	bool operator== (const reverse_iterator& other) const { return element == other.element; }
	bool operator!= (const reverse_iterator& other) { return element != other.element; }
	friend int operator-(reverse_iterator lhs, reverse_iterator rhs) { return rhs.element - lhs.element; }
	reverse_iterator operator+(int pos) { return reverse_iterator(element + pos); }
	reverse_iterator& operator=(reverse_iterator& other) { element = other.element; return *this; }
	char* element;
};
}

